# README #

This is code for the ESP8266 to turn a slow cooker into a sous vide machine that can be controlled over MQTT. A relay is used to turn the heating element on/off, and a DS18B20 sensor for temperature monitoring. Can be updated OTA using ArduinoOTA.

### How do I get set up? ###

* Delete line 8 that includes Secrets.h
* There are 5 config strings you will need to set for this to work. Replace the following with your information surrounded by quotes.
    * WIFI_SSID
    * WIFI_PASS
    * MQTT_SERVER
    * MQTT_USER
    * MQTT_PASS

### Who do I talk to? ###

* Creator - David Junghans (davidjunghans@gmail.com)