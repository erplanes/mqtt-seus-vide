#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <PubSubClient.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <EEPROM.h>

#include <PID_v1.h>
#include <PID_AutoTune_v0.h>

// Contains the WIFI credentials
#include "Secrets.h"


/******************** Wifi Config ********************/
const char* wifiSSID = WIFI_SSID;
const char* wifiPassword = WIFI_PASS;
// Currently not implemented
//const char* otaPassword = OTA_PASS;

/******************** MQTT Config ********************/
const char* mqttServer = MQTT_SERVER;
const char* mqttUser = MQTT_USER;
const char* mqttPassword = MQTT_PASS;

/******************** MQTT Topics ********************/
const char* deviceName = "SmartCooker";
const char* stateSub = "home/SmartCooker/state";
const char* tempSub = "home/SmartCooker/temperature";
const char* setpointSub = "home/SmartCooker/setpoint";
const char* setStatePub = "home/SmartCooker/setState";
const char* setTempPub = "home/SmartCooker/setTemperature";
const char* autotunePub = "home/SmartCooker/startAutotune";
const char* statusPub = "home/SmartCooker/status";
const char* setKpPub = "home/SmartCooker/setKp";
const char* setKiPub = "home/SmartCooker/setKi";
const char* setKdPub = "home/SmartCooker/setKd";


#define HEATER_CONTROL_PIN D5
#define ONE_WIRE_BUS D7

bool masterSwitch = false;
String setCommand = "";
//float setTemp = 0;
double lastMQTTTemp = 0;
long lastTempPubTime = 0;
bool heating = false;

WiFiClient espClient;
PubSubClient client(espClient);

//To make Arduino software autodetect OTA device
WiFiServer TelnetServer(8266);

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature DS18B20(&oneWire);
DeviceAddress tempSensor;

  
// ************************************************
// PID Variables and constants
// ************************************************

//Define Variables we'll be connecting to
double Setpoint;
double Input;
double Output;

volatile long onTime = 0;

// pid tuning parameters
double Kp;
double Ki;
double Kd;

// EEPROM addresses for persisted data
const int SpAddress = 0;
const int KpAddress = 8;
const int KiAddress = 16;
const int KdAddress = 24;

//Specify the links and initial tuning parameters
PID myPID(&Input, &Output, &Setpoint, Kp, Ki, Kd, DIRECT);

// 10 second Time Proportional Output window
int WindowSize = 20000; 
unsigned long windowStartTime;

// ************************************************
// Auto Tune Variables and constants
// ************************************************
byte ATuneModeRemember=2;

double aTuneStep=500;
double aTuneNoise=1;
unsigned int aTuneLookBack=20;

boolean tuning = false;

PID_ATune aTune(&Input, &Output);

const int logInterval = 10000; // log every 10 seconds
long lastLogTime = 0;

String debugString = "";

void setup() {
  pinMode(HEATER_CONTROL_PIN, OUTPUT);
  // We want to start in off position. Relay is unenergized when high.... heh
  digitalWrite(HEATER_CONTROL_PIN, HIGH);
  
  Serial.begin(115200);
  Serial.println("Booting...");

  EEPROM.begin(512);
  
  setupWifi();
  setupOTA();

  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  client.setServer(mqttServer, 1883); //CHANGE PORT HERE IF NEEDED
  client.setCallback(mqttCallback);

  DS18B20.begin();
  if (!DS18B20.getAddress(tempSensor, 0)) 
  {
    Serial.println("Sensor Error");
  }
  DS18B20.setResolution(tempSensor, 12);
  // Request in synchronous mode to get the first reading, then go async
  DS18B20.requestTemperatures();
  DS18B20.setWaitForConversion(false);

  // Initialize the PID and related variables
  LoadParameters();
  myPID.SetTunings(Kp,Ki,Kd);
  Serial.print("Loaded parameters (P,I,D): ");
  Serial.print(Kp);
  Serial.print(",");
  Serial.print(Ki);
  Serial.print(",");
  Serial.println(Kd);
  
  Serial.print("Loaded setpoint: ");
  Serial.print(Setpoint);
  
  myPID.SetSampleTime(1000);
  myPID.SetOutputLimits(0, WindowSize);

  connectMQTT();

  // Publish our current state
  client.publish(stateSub, "OFF");
  char temperatureString[6];
  dtostrf(DS18B20.getTempF(tempSensor), 2, 1, temperatureString);
  client.publish(tempSub, temperatureString);
  dtostrf(Setpoint, 2, 1, temperatureString);
  client.publish(setpointSub, temperatureString);
}

void setupWifi() {
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(wifiSSID);

  WiFi.begin(wifiSSID, wifiPassword);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void setupOTA() {
  //To make Arduino software autodetect OTA device
  TelnetServer.begin();

  // Port defaults to 8266
  //ArduinoOTA.setPort(8266);
  ArduinoOTA.setHostname(deviceName);
  // Uncomment the following as well as the const char* otaPassword line above if you would like password protection
  //ArduinoOTA.setPassword(otaPassword);

  ArduinoOTA.onStart([]() {
    Serial.println("OTA Start");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("OTA End");
    Serial.println("Rebooting...");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("OTA Progress: %u%%\r\n", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("OTA Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("OTA Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("OTA Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("OTA Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("OTA End Failed");
  });
  ArduinoOTA.begin();
}

void mqttCallback(char* topic, byte* payload, unsigned int length) {
  char message_buff[100];
  int i = 0;

  for (i = 0; i < length; i++) {
    message_buff[i] = payload[i];
    Serial.print(".");
  }
  message_buff[i] = '\0';

  Serial.print("MQTT Topic: ");
  Serial.println(topic);
  Serial.print("MQTT Payload: ");
  Serial.println(message_buff);

  debugString += "MQTT Topic: " + String(topic) + "   MQTT Payload: " + String(message_buff) + "<br>";
  
  if (String(topic) == setStatePub) {
    setCommand = String(message_buff);
    if (setCommand == "OFF") {
      masterSwitch = false;
      client.publish(stateSub, "OFF");
    } else if (setCommand == "ON") {
      masterSwitch = true;
      client.publish(stateSub, "ON");

      // Turn the PID algorithm on
      windowStartTime = millis();
      myPID.SetMode(AUTOMATIC);
      Serial.println("PID Algorithm Turned ON");
    }
  } 
  else if (String(topic) == setTempPub) {
    Setpoint = String(message_buff).toFloat();

    SaveParameters();
    
    // Acknowledge the new set point
    client.publish(setpointSub, message_buff);
  } 
  else if (String(topic) == autotunePub) {
    // Need to be at steady state before autotuning
    if (abs(Input - Setpoint) < 0.5) {
      StartAutoTune();
    }
  } 
  else if (String(topic) == setKpPub) {
    Kp = String(message_buff).toFloat();
    SaveParameters();
    myPID.SetTunings(Kp,Ki,Kd);
  } 
  else if (String(topic) == setKiPub) {
    Ki = String(message_buff).toFloat();
    SaveParameters();
    myPID.SetTunings(Kp,Ki,Kd);
  } 
  else if (String(topic) == setKdPub) {
    Kd = String(message_buff).toFloat();
    SaveParameters();
    myPID.SetTunings(Kp,Ki,Kd);
  }
}

void turnOnHeat() {
    digitalWrite(HEATER_CONTROL_PIN, LOW);
    heating = true;
    Serial.println("Set output to: LOW");
}

void turnOffHeat() {
    digitalWrite(HEATER_CONTROL_PIN, HIGH);
    heating = false;
    Serial.println("Set output to: HIGH");
}

//float getTemperature() {
////  Serial.println("Requesting DS18B20 temperature...");
//  float temp;
//  do {
//    DS18B20.requestTemperatures(); 
//    temp = DS18B20.getTempFByIndex(0);
//    delay(100);
//  } while (temp == 85.0 || temp == (-127.0));
//  return temp;
//}

// ************************************************
// Autotune
// ************************************************

void StartAutoTune()
{
   // Remember the mode we were in
   ATuneModeRemember = myPID.GetMode();

   // set up the auto-tune parameters
   aTune.SetNoiseBand(aTuneNoise);
   aTune.SetOutputStep(aTuneStep);
   aTune.SetLookbackSec((int)aTuneLookBack);
   tuning = true;

   client.publish(statusPub, "Autotune started");
}
void FinishAutoTune()
{
   tuning = false;

   // Extract the auto-tune calculated parameters
   Kp = aTune.GetKp();
   Ki = aTune.GetKi();
   Kd = aTune.GetKd();

   // Re-tune the PID and revert to normal control mode
   myPID.SetTunings(Kp,Ki,Kd);
   myPID.SetMode(ATuneModeRemember);
   
   // Persist any changed parameters to EEPROM
   SaveParameters();
}

// ************************************************
// Save/Load Paramaters from EEPROM
// ************************************************
void SaveParameters()
{
   if (Setpoint != EEPROM_readDouble(SpAddress))
   {
      EEPROM_writeDouble(SpAddress, Setpoint);
   }
   if (Kp != EEPROM_readDouble(KpAddress))
   {
      EEPROM_writeDouble(KpAddress, Kp);
   }
   if (Ki != EEPROM_readDouble(KiAddress))
   {
      EEPROM_writeDouble(KiAddress, Ki);
   }
   if (Kd != EEPROM_readDouble(KdAddress))
   {
      EEPROM_writeDouble(KdAddress, Kd);
   }

   EEPROM.commit();
}
void LoadParameters()
{
  // Load from EEPROM
   Setpoint = EEPROM_readDouble(SpAddress);
   Kp = EEPROM_readDouble(KpAddress);
   Ki = EEPROM_readDouble(KiAddress);
   Kd = EEPROM_readDouble(KdAddress);

   Serial.println(Setpoint);
   Serial.println(Kp);
   Serial.println(Ki);
   Serial.println(Kd);
   
   // Use defaults if EEPROM values are invalid
   if (isnan(Setpoint))
   {
     Setpoint = 60;
   }
   if (isnan(Kp))
   {
     Kp = 850;
   }
   if (isnan(Ki))
   {
     Ki = 0.5;
   }
   if (isnan(Kd))
   {
     Kd = 0.1;
   }  
}
void EEPROM_writeDouble(int address, double value)
{
   byte* p = (byte*)(void*)&value;
   for (int i = 0; i < sizeof(value); i++)
   {
      EEPROM.write(address++, *p++);
   }
}
double EEPROM_readDouble(int address)
{
   double value = 0.0;
   byte* p = (byte*)(void*)&value;
   for (int i = 0; i < sizeof(value); i++)
   {
      *p++ = EEPROM.read(address++);
   }
   return value;
}

// ************************************************
// PID Control loop
// ************************************************
void doControl()
{ 
  if (tuning) // run the auto-tuner
  {
     if (aTune.Runtime()) // returns 'true' when done
     {
        FinishAutoTune();
     }
  }
  else // Execute control algorithm
  {
     myPID.Compute();
  }
  
  // Time Proportional relay state is updated regularly via timer interrupt.
  onTime = Output; 

  long now = millis();
  // Set the output
  // "on time" is proportional to the PID output
  if(now - windowStartTime > WindowSize)
  { //time to shift the Relay Window
     windowStartTime += WindowSize;
  }
  
  if((Output > 100) && (Output > (now - windowStartTime)))
  {
    if (!heating)
      turnOnHeat();
  }
  else
  {
    Serial.print("now,windowStartTime");
    Serial.print(now);
    Serial.print(",");
    Serial.println(windowStartTime);
    if (heating)
      turnOffHeat();
  }
}

// prepare a web page to be send to a client (web browser)
String prepareHtmlPage()
{
  String htmlPage =
     String("HTTP/1.1 200 OK\r\n") +
            "Content-Type: text/html\r\n" +
            "Connection: close\r\n" +  // the connection will be closed after completion of the response
            "Refresh: 10\r\n" +  // refresh the page automatically every 5 sec
            "\r\n" +
            "<!DOCTYPE HTML>" +
            "<html>" +
            "Input:  " + String(Input) + "<br>Setpoint:" + String(Setpoint) + "<br>Output: " + String(Output) + "<br>" +
            "Kp:  " + String(Kp) + "<br>Ki:" + String(Ki) + "<br>Kd: " + String(Kd) + "<br>" +
            "Autotuning:  " + (tuning ? "Yes" : "No") + "<br>" +
            "Debug log:<br>" + debugString +
            "</html>" +
            "\r\n";
  return htmlPage;
}

// ************************************************
// Main Loop
// ************************************************
void loop() {
  if (!client.connected()) {
    connectMQTT();
  }
  client.loop();

  Input = DS18B20.getTempF(tempSensor);
  DS18B20.requestTemperatures(); // prime the pump for the next one - but don't wait

  long now = millis();
  char temperatureString[6];
  // Only publish our temperature if it's changed by at least .1 and it's been at least 30 seconds
  if ((abs(Input - lastMQTTTemp) > 0.1) && (now - lastTempPubTime) > 30000) {
    lastMQTTTemp = Input;
    lastTempPubTime = now;
    // convert temperature to a string with two digits before the comma and 2 digits for precision
    dtostrf(lastMQTTTemp, 2, 1, temperatureString);
    client.publish(tempSub, temperatureString);
  }

  if (masterSwitch) {
    doControl();

    // Log to serial port in csv format
    if (millis() - lastLogTime > logInterval)  
    {
      lastLogTime = millis();
      Serial.print(Input);
      Serial.print(",");
      Serial.print(Setpoint);
      Serial.print(",");
      Serial.println(Output);
    }
  } else if (heating) {
    turnOffHeat();
  }

  WiFiClient webclient = TelnetServer.available();
  // wait for a client (web browser) to connect
  if (webclient)
  {
    Serial.println("\n[Client connected]");
    while (webclient.connected())
    {
      // read line by line what the client (web browser) is requesting
      if (webclient.available())
      {
        String line = webclient.readStringUntil('\r');
        Serial.print(line);
        // wait for end of client's request, that is marked with an empty line
        if (line.length() == 1 && line[0] == '\n')
        {
          webclient.println(prepareHtmlPage());
          break;
        }
      }
    }
    delay(1); // give the web browser time to receive the data

    // close the connection:
    webclient.stop();
    Serial.println("[Client disonnected]");
  }
  
  ArduinoOTA.handle();
}

void connectMQTT() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(deviceName, mqttUser, mqttPassword)) {
      Serial.println("connected");

      client.subscribe(setStatePub);
      client.loop();
      client.subscribe(setTempPub);
      client.loop();
      client.subscribe(setKpPub);
      client.loop();
      client.subscribe(setKiPub);
      client.loop();
      client.subscribe(setKdPub);
      client.loop();
      client.subscribe(autotunePub);
      client.loop();
      client.subscribe(statusPub);
      client.loop();
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
